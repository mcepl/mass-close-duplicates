/*jslint rhino: true, forin: true, onevar: false, browser: true, evil: true, laxbreak: true, undef: true, nomen: true, eqeqeq: false, bitwise: true, maxerr: 1000, immed: false, white: false, plusplus: false, regexp: false, undef: false */
// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php
"use strict";
// https://bugzilla.redhat.com/show_bug.cgi?id=543165

var xmlrpc = require("xmlrpc");
var myConfig = require("simple-storage").storage;
var Request = require("request").Request;
var passUtils = require("passwords");
var timer = require("timer");
var prompts = require("prompts");
var urlMod = require("url");
var tabs = require("tabs");
var selfMod = require("self");

var ourRealm = "massCloseDeduplicate";
var ourPageRE = new RegExp("buglist\\.cgi\\?.*\\&tweak=1");
var howManyAtOnce = 10;

function dateToISO8601(date){
    function leadingZero(n){
        // pads a single number with a leading zero. Heh.
        if (n.length==1) {
            n = "0" + n;
        }
        return n;
    }

    var year = date.getYear();
    var month = leadingZero(date.getMonth());
    var day = leadingZero(date.getDate());
    var time = leadingZero(date.getHours()) +
        ":" + leadingZero(date.getMinutes()) +
        ":" + leadingZero(date.getSeconds());

    var converted = year+month+day+"T"+time;
    return converted;
}

/**
 *
 */
function DuplicateQueryPage(tab) {
    this.doc = tab.contentDocument;
    this.win = tab.contentWindow;
    this.reqCounter = 0;
    this.reqQueue = [];
    this.reqDupID = null;
    this.XMLRPCURLs = JSON.parse(selfMod.data.load("XMLRPCdata.json"));

    var headerDiv = this.doc.getElementById("header");
    // Protection against premature loading
    if (!headerDiv) {
        return null;
    }

    var loginArrDOM = headerDiv.
        getElementsByTagName("ul")[0].getElementsByTagName("li");
    var loginArr = loginArrDOM[loginArrDOM.length - 1].textContent.split("\n");
    this.login = loginArr[loginArr.length - 1].trim();

    // FIXME throw an exception when login not found

    this.password = passUtils.getPassword(this.login,
        this.win.location.hostname, ourRealm);
    if (!this.password) {
        this.password = prompts.prompt("Enter your Bugzilla password");
        passUtils.setLogin(this.login, this.password,
            this.win.location.hostname, ourRealm);
     }

    var origButton = this.doc.getElementsByName("check_all")[0];
    // If there are no bugs found, there is no origButton
    if (origButton) {
        var inpActionButton = this.createNewButton(origButton.nextSibling,
            "dupeid_action","Close DUPs",this.deduplicateFromTable);
        this.createNewButton(inpActionButton,"dupe_of","Dup#",this.duplicateOf);
        this.createNewButton(inpActionButton,"dupeid_inp","","text");
    }
}

/**
 * Callback function for the XMLRPC request
 *
 * @param ret object with xmlhttprequest response
 *                with attributes:
 *                + status -- int return code
 *                + statusText
 *                + responseHeaders
 *                + responseText
 */
DuplicateQueryPage.prototype.callBack = function() {
    console.log("reqCounter = " + this.reqCounter);
    this.reqCounter--;
    if (this.reqCounter <= 0) {
        this.processReqQueue();
    }
};

/**
 * very page specific function to get bug ID# for a bug in the mass-change list
 *
 * @param el DOM Element
 * @return Number with ID# or 0 if something wrong happens.
 */
DuplicateQueryPage.prototype.getElementIDForElem = function(el) {
    var ID = 0;
    if (el.hasAttribute("name")) {
        var IDstr = /id_([0-9]*)/.exec(el.getAttribute("name"))[1];
        ID = parseInt(IDstr,10);
    }
    return ID;
};

/**
 *
 */
DuplicateQueryPage.prototype.duplicateOf = function() {
    var that = this;
    var testElem = this.doc.querySelector("tr.bz_bugitem input[type='checkbox']:checked");
    // FIXME and what if I won't find it?
    var id = this.getElementIDForElem(testElem);

    Request({
        url:"https://" + this.win.location.hostname + "/show_bug.cgi?ctype=xml&id=" + id,
        onComplete: function(response) {
            if (response.status == 200) {
                var bugzilla = response.xml;
                var dupeIDs = bugzilla.getElementsByTagName("dup_id");
                if (dupeIDs) {
                    var dupeID = parseInt(dupeIDs[0].textContent,10);
                    that.doc.getElementById("dupeid_inp").value = dupeID;
                    testElem.removeAttribute("checked");
                }
            }
        }
    }).get();
};

/**
 *
 */
DuplicateQueryPage.prototype.createNewButton = function (where,id,value,cb) {
    var nameVal = "";
    var that = this;
    var type = "";

    if (typeof cb == 'function') {
        type = "button";
    } else {
        type = "text";
        nameVal = id;
    }

    var butt = this.doc.createElement("input");
    butt.setAttribute("type",type);
    butt.setAttribute("value",value);
    butt.setAttribute("id",id);
    if (type == "text") {
        butt.setAttribute("name",nameVal);
        butt.setAttribute("size",12);
    }

    where.parentNode.insertBefore(butt,where);
    where.parentNode.insertBefore(this.doc.createTextNode("\u00A0"),where);


    if (type == "button") {
        butt.addEventListener("click",function () {
            cb.call(that);
        },false);
    }

    return butt;
};

/**
 * The worker function -- call XMLRPC to fix MIME type of the
 * particular attachment
 *
 * @param id integer with the attachment id to be fixed
 * @param type string with the new MIME type, e.g. "text/plain"

    "closeBug($params);"
        Close a current Bugzilla bug report with a specific resolution.
        This will eventually be done in Bugzilla/Bug.pm instead and is
        meant to only be a quick fix. Please use RedHat.changesStatus to
        changed to an opened state.  This method will change the bug
        report's status to CLOSED.

        Params:
            $params => {
                id => '<bug_id>',             # ID of bug report to close
                resolution => '<resolution>', # Valid Bugzilla resolution
                                                to transition the report into.
                                                DUPLICATE requires 'dup_id'
                                                to be passed in.
                dup_id => '<bug_id>',         # Bugzilla report ID that
                                                this bug is being closed as
                                                duplicate of. Requires
                                                'resolution' to be DUPLICATE.
                fixed_in => '<fixed_in>',     # OPTIONAL String
                                                representing version of
                                                product/component
                                                that bug is fixed in.
                comment => '<comment>',       # OPTIONAL Text string
                                                containing comment to add.
                isprivate => 1 or 0,          # OPTIONAL Whether the
                                                comment will be private to the
                                                'private_comment' Bugzilla
                                                group.
                                                Default: false
                nomail => 1 or 0,             # OPTIONAL Flag that is
                                                either 1 or 0 if you want
                                                email to be sent or not for
                                                this change
            }
 */
DuplicateQueryPage.prototype.deduplicate = function deduplicate(ids, dID) {
    var that = this;
    var XMLRPCURLs = JSON.parse(selfMod.data.load("XMLRPCdata.json"));
    var msg = new xmlrpc.XMLRPCMessage("Bug.update"); // upstream call!!!
    msg.addParameter( {
        'ids' : ids,
        'nomail': 1,
        'Bugzilla_login': this.login,
        'Bugzilla_password': this.password,
        updates: {
            'status': 'CLOSED',
            'resolution': "DUPLICATE",
            'dupe_id': dID
        }
    });
    Request({
        url: XMLRPCURLs[this.win.location.hostname].url, // orig. XMLRPCurl
        onComplete: function(response) {
            if (response.status == 200) {
                that.callBack();
            } else {
                throw new Error("HTTP Request failed!");
            }
        },
        content: msg.xml(),
        contentType: "text/xml"
    }).post();
    this.reqCounter++;
};

DuplicateQueryPage.prototype.processReqQueue = function processReqQueue() {
    var howMany = 0;
    if (this.reqQueue.length <= 0) {
        this.reqDupID = null;
         timer.setTimeout(tabs.activeTab.location.reload,
            1000);
        return ;
    } else {
        if (this.reqQueue.length >= howManyAtOnce) {
            howMany = howManyAtOnce;
        } else {
            howMany = this.reqQueue.length;
        }
        var part = this.reqQueue.splice(0, howMany);
        part.forEach(function(id) {
            this.deduplicate(id, this.reqDupID);
        }, this);

        var that = this;
        timer.setTimeout(that.processReqQueue,
            1000);
    }
};

/**
 * Picks the list of bugs to be closed, collects ID#s and calls this.deduplicate
 * to do actual work
 *
 * @return none
 */
DuplicateQueryPage.prototype.deduplicateFromTable = function() {
    var dupeIDstr = this.doc.getElementById("dupeid_inp").value;
    if (!dupeIDstr) {
        return ;
    }

    var testElems = this.doc.
        querySelectorAll("table.bz_buglist tr.bz_bugitem input[type='checkbox']:checked");

    var idsToClose = Array.map(testElems, function (elem) {
        return this.getElementIDForElem(elem);
    }, this);

    this.reqQueue = this.reqQueue.concat(idsToClose);
    this.reqDupID = dupeIDstr;
    this.processReqQueue();
};

tabs.onReady = function (tab) {
    if (ourPageRE.test(tab.contentWindow.location.href)) {
        var curPage = new DuplicateQueryPage(tab);
    }
};
